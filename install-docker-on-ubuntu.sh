# install-docker-on-ubuntu
#Check apt update
 sudo apt-get update

#Get Certificates
 sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg \
    lsb-release

#Get GPG key
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg

#Input GPG into docker.list
echo \
  "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

#Check apt update
 sudo apt-get update

 #Install docker from Official Site
 sudo apt-get install docker-ce docker-ce-cli containerd.io
