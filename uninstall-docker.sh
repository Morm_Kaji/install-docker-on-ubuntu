#Check Docker
dpkg -l | grep -i docker

#Search Docker
sudo apt-get purge -y docker-engine docker docker.io docker-ce docker-ce-cli

#Delete Docker Docker.io Docker-ce
sudo apt-get autoremove -y --purge docker-engine docker docker.io docker-ce  

sudo rm -rf /var/lib/docker /etc/docker
sudo rm /etc/apparmor.d/docker
sudo groupdel docker
sudo rm -rf /var/run/docker.sock
sudo rm /etc/apt/sources.list.d/archive_uri-https_apt_dockerproject_org_repo_-hirsute.list
sudo rm /etc/apt/sources.list.d/docker.list
